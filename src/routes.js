import React from 'react'
import { Router, Route, hashHistory } from 'react-router'

import App from 'pages/App'

import RegistrationForm from 'pages/examples/RegistrationForm'

const routes = (
  <Router history={hashHistory}>
    <Route component={App}>
      <Route path="/" component={RegistrationForm}/>
    </Route>
  </Router>
)

export default routes
