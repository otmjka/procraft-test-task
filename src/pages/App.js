import React, { PropTypes, Component } from 'react'

class App extends Component {
  render() {
    const styles = require('./App.scss')
    return (
      <div>
        <div className={styles.appContent}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

App.propTypes = {
  children: PropTypes.any
}

export default App
