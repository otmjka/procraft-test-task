import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { show as showResults } from '../../redux/modules/submission'

import { reduxForm } from 'redux-form'
import styles from './RegistrationForm.scss'
import Select from 'react-select'
import { Dropdown, MenuItem } from 'react-bootstrap'
import autobind from 'autobind-decorator'
import IMG from '../../img/maket.png'

import SubmissionResults from '../../components/SubmissionResults'

import MaskedFormControl from 'react-maskedinput'
import cn from 'classnames'

import FLAG_RU from 'flag-icon-css/flags/4x3/ru.svg'
import FLAG_UA from 'flag-icon-css/flags/4x3/ua.svg'
import FLAG_BY from 'flag-icon-css/flags/4x3/by.svg'
import FLAG_KZ from 'flag-icon-css/flags/4x3/kz.svg'

export const fields = [ 'firstName', 'lastName', 'profession', 'phone' ]

const COUNTRIES_CODES = [
  {
    id: 0, 
    label: 'Россия',
    flag: FLAG_RU,
    code: '+7',
    mask: '+7 111 111-11-11',
    placeholder: '+7 495 123-45-67'
  },
  {
    id: 1, 
    label: 'Белорусь',
    flag: FLAG_BY,
    code: '+375',
    mask: '+375 11 111-11-11',
    placeholder: '+375 17 123-45-67'
  },
  {
    id: 2, 
    label: 'Украина',
    flag: FLAG_UA,
    code: '+380',
    mask: '+380 11 111-11-11',
    placeholder: '+380 32 123-45-67'
  },
  {
    id: 3, 
    label: 'Казахстан',
    flag: FLAG_KZ,
    code: '+7',
    mask: '+7 111 111-11-11',
    placeholder: '+7 710 123-45-67'

  }
]

const options = [
    { value: 0, label: 'Парикмахер-0' },
    { value: 1, label: 'Парикмахер-1' },
    { value: 2, label: 'Парикмахер-2' },
    { value: 3, label: 'Парикмахер-3' },
    { value: 4, label: 'Парикмахер-4' },
    { value: 5, label: 'Парикмахер-5' },
    { value: 6, label: 'Парикмахер-6' }
]
const EMPTY_FILELD = 'не должно быть пустым'
class RegistrationForm extends Component {
  validation(values, submit, resolve, reject) {
    const result = {}
    let existsError = false
    
    if (values.firstName == null || !values.firstName.length) {
      result.firstName = EMPTY_FILELD
      existsError = true
    }

    if (values.lastName == null || !values.lastName.length) {
      result.lastName = EMPTY_FILELD
      existsError = true
    }

    if (values.profession == null || values.profession < 0) {
      result.profession = EMPTY_FILELD
      existsError = true
    }

    if (values.phone == null || values.phone < 0) {
      result.phone = EMPTY_FILELD
      existsError = true
    }

    return existsError 
      ? reject(result)
      : resolve(submit(values))
  }
  render() {
    const {
      fields,
      handleSubmit,
      resetForm,
      submitting,

      submit
      } = this.props
    const formProps = {
      fields, handleSubmit, resetForm, submitting,

      professionList: options,
      countriesCodes: COUNTRIES_CODES,
      onSubmit: values => new Promise((resolve, reject) => {
        setTimeout(() => {
          this.validation(values, submit, resolve, reject)
        }, 500)
      })
    }
    return (
      <div className="container">
        <RegistrationFormDump {...formProps} />
        <SubmissionResults />
      </div>
    )
  }
}

// rf - register form

class RegistrationFormDump extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      inputValue: '',
      selectedCountry: 0
    }
  }

  @autobind
  onChange(value) {
    this.setState({ value })
  }
  
  @autobind
  onInputChange(inputValue) {
    this.setState({ inputValue })
  }

  @autobind
  onBlurSelect() {
    this.setState({ inputValue: '' })
  }

  @autobind
  optionRendered({ label }) {
    const { inputValue } = this.state 
    let bold = (label.indexOf(inputValue) === 0)
      ? inputValue
      : void 0


    return (
      <div className="Select-option">
        {bold && <span className={styles.bold}>{bold}</span>}
        {bold ? label.substr(bold.length) : label}
      </div>
    )
  }
  
  onClickFlagSelect(selectedCountry) {
    this.setState({ selectedCountry })
  }

  static propTypes = {
    boldformHeading: PropTypes.string.isRequired,
    formHeading: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    userNamePlaceholder: PropTypes.string.isRequired,

    userLastName: PropTypes.string.isRequired,
    userLastNamePlaceholder: PropTypes.string.isRequired,

    userProfession: PropTypes.string.isRequired,
    userProfessionPlaceHolder: PropTypes.string.isRequired,
    userPhone: PropTypes.string.isRequired,

    registrationLabel: PropTypes.string.isRequired,

    // Validation decorator
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,

    // profession
    professionList: PropTypes.array.isRequired,

    //phone
    countriesCodes: PropTypes.array.isRequired
  }
  
  static defaultProps = {
    boldformHeading: 'Зарегестрируйтесь',
    formHeading: ' и начните продавать услуги через интернет сегодня',
    
    userName: 'Имя',
    userNamePlaceholder: 'Сергей',

    userLastName: 'Фамилия',
    userLastNamePlaceholder: 'Миргород',

    userProfession: 'Профессия',
    userProfessionPlaceHolder: 'Скрипач',
    userPhone: 'Телефон',

    registrationLabel: 'Зарегестрироваться'
  }

  render() { 
    const {
      boldformHeading, formHeading,  
      userName, userNamePlaceholder,
      userLastName, userLastNamePlaceholder,
      userProfession, userProfessionPlaceHolder,
      userPhone, registrationLabel,

      professionList,
      countriesCodes, 

      fields: { firstName, lastName, profession, phone },
      handleSubmit
    } = this.props

    const { /*value,*/ selectedCountry } = this.state

    const { flag, mask, placeholder } = countriesCodes[selectedCountry]
    const phoneWrProps = {
      className: cn('row', styles['rf-phone__wr'])
    }
    
    const maskedProps = {
      type: 'text',
      name: 'phoneNumber',
      mask,
      placeholder,
      className: cn('form-control', styles['rf-phone-input']),
      ...phone
    }
    
    const selProps = {
      ...profession,
      options: professionList,
      optionRenderer: this.optionRendered,
      // value,
      // onChange: ()=>{profession.onChange this.onChange},
      onInputChange: this.onInputChange,
      onBlur: this.onBlurSelect,
      placeholder: userProfessionPlaceHolder
    }

    const selWrProps = {
      className: cn('row', styles['rf-prof__rw'])
    }
    
    const labelProps = {
      className: cn('col-xs-4', 'control-label', styles['rf-label'])
    }

    const nameContProps = {
      className: cn('col-xs-6', styles['rf-name-field__wr'])
    }

    const lastNameContProps = {
      className: cn('col-xs-6', styles['rf-lname-field__wr']) 
    }
    
    const nameFiedProps = {
      type: 'text',
      className: cn('form-control', styles['rf-name-field-input']),
      placeholder: userNamePlaceholder,
      ...firstName
    }

    const lnameFiedProps = {
      type: 'text',
      className: cn('form-control', styles['rf-name-field-input']),
      placeholder: userLastNamePlaceholder,
      ...lastName
    }

    const genInfProps = {
      className: cn('row', styles['fr-gen-info__wr'])
    }
    const submitInnerProps = {
      className: cn('col-xs-12', styles['fr-submit__inner'])
    }
    const submitProps = {
      className: cn('btn', 'btn-primary', styles['fr-submit']),
      type: 'submit'
    }
    return (
      <div className={styles['bs-example']} >
        {true && <img src={IMG} className={styles.maketimg} />}
        <div className={cn({
          [styles['rf-heading']]: true,
          [styles['rf-heading__mb']]: true
        })}>
          <span className={styles.bold}>
            {boldformHeading}
          </span>
          {formHeading}
        </div>
        {/*заголовок*/}
        <form onSubmit={handleSubmit(this.props.onSubmit)}>
          
          {/*Имя*/}
          <div {...genInfProps}>
            <div {...nameContProps}>
              <div className={cn('form-group', { 'has-error': firstName.touched && firstName.error })}>
                <label {...labelProps}>{userName}</label>
                <input {...nameFiedProps} />
                <ErrorTip field={firstName}/>
              </div>
            </div>

            {/*Фамилия*/}
            <div {...lastNameContProps}>
              <div className={cn('form-group', { 'has-error': lastName.touched && lastName.error })}>
                <label {...labelProps}>{userLastName}</label>
                <input {...lnameFiedProps} />
                <ErrorTip field={lastName}/>
              </div>
            </div>
          </div>

          {/*Профессия*/}
          <div {...selWrProps}>
            <div className="col-xs-12">
              <div className={cn('form-group', { 'has-error': profession.touched && profession.error })}>
                  <label {...labelProps}>{userProfession}</label>
                  <Select {...selProps} />
                  <ErrorTip field={profession}/>
              </div>
            </div>
          </div>

          {/*Телефон*/}
          <div {...phoneWrProps}>
            <div className="col-xs-12">
              <div className={cn('form-group', { 'has-error': phone.touched && phone.error })}>
                <label {...labelProps}>{userPhone}</label>
                <div className="input-group">
                  <div className="input-group-btn">
                    <Dropdown id={`dropdown-basic ${userPhone}1`}>
                      <Dropdown.Toggle className={styles['rf-phone-selected-flag']}>
                        <img className={styles['rf-flag-img']} src={countriesCodes[this.state.selectedCountry].flag} />
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {countriesCodes.map(({ id, flag, label })=>(
                          <MenuItem onClick={this.onClickFlagSelect.bind(this, id)} key={id} eventKey={id} active={id === this.state.selectedCountry}>
                            <img className={styles['rf-flag-img']} src={flag} />
                            {label}
                          </MenuItem>))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  <MaskedFormControl {...maskedProps} />
                </div>
                <ErrorTip field={phone}/>
              </div>

            </div>
          </div>
          {/*Кнопа регистрации*/}
          <div className="row">
            <div {...submitInnerProps}>
              <button {...submitProps}>{registrationLabel}</button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const ErrorTip = ({ field })=>{
  return field.touched && field.error 
    ? (<div className={styles['rf-error-tip']}>{field.error}</div>)
    : null
}

export default connect(undefined, { submit: showResults })(reduxForm({
  form: 'registerForm',
  fields
})(RegistrationForm))
